const express = require('express');
const favicon = require('express-favicon');
const app = express();
const path = require('path');

//settings
app.set('host', process.env.HOST || '0.0.0.0');
app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(favicon(__dirname + '/public/img/favicon.ico'));

//middlewares

//routes
app.use(require('./routes/index'));

//static files
app.use(express.static(path.join(__dirname, 'public')));

//main
app.listen(app.get('port'), app.get('host'), () => {
    console.log('Server on port', app.get('port'));
});