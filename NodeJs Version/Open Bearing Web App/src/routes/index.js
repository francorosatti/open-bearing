const express = require('express');
const router = express.Router();
const http = require('http');

router.get('/', (req, res) => {
    res.render('index.html', { page: 'home', title: 'Open Bearing' });
});

router.get('/contact', (req, res) => {
    res.render('contact.html', { page: 'contact', title: 'Open Bearing - Contact' });
});

router.get('/bearings', (req, res) => {
    res.render('bearings.html', { page: 'bearings', title: 'Open Bearing - Bearings' });
});

router.get('/api/table', (req, res) => {
    const { filter } = req.query;
    if (filter == null || filter == undefined || filter == "" || filter.length <= 0){
        res.render('table.html', { bearings: [] });
    }else{
        var options = {
            host: 'localhost',
            port: '4001',
            path: `/bearings?filter=${filter}`
        };
        callback = function (response) {
            var str = '';
            response.on('data', function (chunk) {
                str += chunk;
            });
            response.on('end', function () {
                res.render('table.html', { bearings: JSON.parse(str) });
            });
        }
        http.request(options, callback).end();
    }
});

module.exports = router;