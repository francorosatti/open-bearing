const mysqlConnection = require('../services/database');

module.exports = {
    get: async (request, h) => {
        mysqlConnection.query("SELECT bookstore.books.*,  bookstore.authors.name AS author FROM bookstore.books INNER JOIN bookstore.authors ON bookstore.books.authorid = bookstore.authors.id;", function (err, rows, fields) {
            if (err) throw err;
            return h.response([{ title: "LOTR I", author: "J.R.R. Tolkien", year: "1954" },
            { title: "LOTR II", author: "J.R.R. Tolkien", year: "1954" },
            { title: "LOTR III", author: "J.R.R. Tolkien", year: "1955" }
            ]).code(200);
        });
    }
}