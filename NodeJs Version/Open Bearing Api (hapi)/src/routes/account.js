const controller = require('../controllers/accountController');

module.exports = [
    { method: 'GET', path: '/account/login', config: { 
        handler: controller.login
    }}
];