var test = require('./test');
var user = require('./user');
var book = require('./book');
var account = require('./account');

module.exports = [].concat(test, user, book, account);