const controller = require('../controllers/userController');

module.exports = [
    { method: 'GET', path: '/users', config: { 
        handler: controller.get
    }}
];