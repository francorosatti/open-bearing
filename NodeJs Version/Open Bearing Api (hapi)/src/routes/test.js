const controller = require('../controllers/testController');
const Joi = require('@hapi/joi');

module.exports = [
    { method: 'GET', path: '/', config: { 
        handler: controller.get
    }},
    { method: 'GET', path: '/create', config: { 
        handler: controller.create
    }}
];