const controller = require('../controllers/bookController');

module.exports = [
    { method: 'GET', path: '/books', config: { 
        handler: controller.get
    }}
];