const Hapi = require('@hapi/hapi');
var routes = require('./routes');

const server = Hapi.server({
  port: process.env.PORT || 3001,
  host: 'localhost',
  app: {}
});

const iniciarServer = async () => {
  try {
    await server.route(routes);
    await server.start();
    console.log(`Servidor corriendo en: ${server.info.uri}`);
  } catch (error) {
    console.log('Error al iniciar el servidor Hapi');
  }
};

iniciarServer();