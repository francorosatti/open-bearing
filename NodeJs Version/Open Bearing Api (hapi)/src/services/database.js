const mysql = require('mysql');
const mysqlConnection = mysql.createConnection({
    host: "localhost",
    user: "bookstore_admin",
    password: "abcd1234",
    database: 'bookstore'
});

mysqlConnection.connect(function(err){
    if(err){
        console.log(err);
        return
    } else {
        console.log('Connection Successful');
    }
});

module.exports = mysqlConnection;