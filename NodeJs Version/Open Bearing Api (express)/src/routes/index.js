const express = require('express');
const router = express.Router();
const mysqlConnection = require('../services/database');

function isInt(value) {
    return !isNaN(value) &&
        parseInt(Number(value)) == value &&
        !isNaN(parseInt(value, 10));
}

function getFilterQuery(filter) {
    let queryFilter = "";
    let splited_filter = filter.split(" ");
    for (let i = 0; i < splited_filter.length; i++) {
        if (splited_filter[i] == null || splited_filter[i] == undefined || splited_filter[i].length == "" || splited_filter[i].length <= 0) continue;
        if (queryFilter !== "") queryFilter += "AND"
        queryFilter += "(b.code LIKE '%" + splited_filter[i] + "%' OR m.name LIKE '%" + splited_filter[i] + "%')"
    }
    return queryFilter;
}

function firstAuthorizationStage(email, apikey) {
    if (email == null || email == undefined || email == "" || email.length <= 0) return false;
    if (apikey == null || apikey == undefined || apikey == "" || apikey.length <= 0) return false;
    return true;
}

router.get('/manufacturers', (req, res) => {
    console.log('/manufacturers');
    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!firstAuthorizationStage(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    const filter = req.query.filter;
    if (filter != null && filter != undefined && filter != "" && filter.length > 0) {
        mysqlConnection.query(`SELECT m.* FROM users as u INNER JOIN manufacturers AS m ON (u.email='${email}' AND u.apikey='${apikey}' AND u.enabled > 0) WHERE m.name LIKE '%${filter}%' LIMIT 50;`, (err, rows, fields) => {
            if (err) throw err;
            res.json(rows);
        });
    } else {
        mysqlConnection.query(`SELECT m.* FROM users AS u INNER JOIN manufacturers AS m ON (u.email='${email}' AND u.apikey='${apikey}' AND u.enabled > 0) LIMIT 50;`, (err, rows, fields) => {
            if (err) throw err;
            res.json(rows);
        });
    }
});

router.get('/manufacturers/:id', (req, res) => {
    console.log('/manufacturers/id');
    const id = req.params.id;
    if (!isInt(id)) {
        res.sendStatus(400);
        return;
    }

    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!firstAuthorizationStage(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    mysqlConnection.query(`SELECT m.* FROM users AS u INNER JOIN manufacturers AS m ON (u.email='${email}' AND u.apikey='${apikey}' AND u.enabled > 0) WHERE m.id = ${id};`, (err, rows, fields) => {
        if (err) throw err;
        res.json(rows);
    });
});

router.get('/bearings', (req, res) => {
    console.log('/bearings');

    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!firstAuthorizationStage(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    const filter = req.query.filter;
    if (filter != null && filter != undefined && filter != "" && filter.length > 0) {
        mysqlConnection.query(`SELECT b.*, m.name AS manufacturer FROM bearings AS b INNER JOIN manufacturers AS m ON b.manufacturerid = m.id INNER JOIN users AS u ON (u.email='${email}' AND u.apikey='${apikey}' AND u.enabled > 0) WHERE (${getFilterQuery(filter)}) LIMIT 50;`, (err, rows, fields) => {
            if (err) throw err;
            res.json(rows);
        });
    } else {
        mysqlConnection.query(`SELECT b.*, m.name AS manufacturer FROM bearings AS b INNER JOIN manufacturers AS m ON b.manufacturerid = m.id INNER JOIN users AS u ON (u.email='${email}' AND u.apikey='${apikey}' AND u.enabled > 0) LIMIT 50;`, (err, rows, fields) => {
            if (err) throw err;
            res.json(rows);
        });
    }
});

router.get('/bearings/:id', (req, res) => {
    console.log('/bearings/id');

    const id = req.params.id;
    if (!isInt(id)) {
        res.sendStatus(400);
        return;
    }

    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!firstAuthorizationStage(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    mysqlConnection.query(`SELECT b.*, m.name AS manufacturer FROM bearings AS b INNER JOIN manufacturers AS m ON b.manufacturerid = m.id INNER JOIN users AS u ON (u.email='${email}' AND u.apikey='${apikey}' AND u.enabled > 0) WHERE b.id = ${id};`, (err, rows, fields) => {
        if (err) throw err;
        res.json(rows);
    });
});

module.exports = router;