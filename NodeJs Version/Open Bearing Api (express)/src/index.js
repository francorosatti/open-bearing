const express = require('express');
const app = express();
const path = require('path');

//settings
app.set('port', process.env.PORT || 4001);

//middlewares


//routes
app.use(require('./routes/index'));

//main
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});