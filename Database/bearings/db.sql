-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema open_bearing
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema open_bearing
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `open_bearing` DEFAULT CHARACTER SET utf8 ;
USE `open_bearing` ;

-- -----------------------------------------------------
-- Table `open_bearing`.`manufacturers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `open_bearing`.`manufacturers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `open_bearing`.`bearings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `open_bearing`.`bearings` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(80) NOT NULL,
  `bpfi` DOUBLE NULL,
  `bpfo` DOUBLE NULL,
  `bsf` DOUBLE NULL,
  `ftf` DOUBLE NULL,
  `manufacturerid` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unique_manufacturer_code` (`manufacturerid` ASC, `code` ASC),
  INDEX `index_code` (`code` ASC),
  INDEX `index_manufacturer` (`manufacturerid` ASC),
  CONSTRAINT `fk_manufacturer_bearing`
    FOREIGN KEY (`manufacturerid`)
    REFERENCES `open_bearing`.`manufacturers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `open_bearing`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `open_bearing`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(80) NOT NULL,
  `apikey` VARCHAR(45) NOT NULL,
  `enabled` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  UNIQUE INDEX `apikey_UNIQUE` (`apikey` ASC))
ENGINE = InnoDB;

CREATE USER 'openbearing_admin' IDENTIFIED BY 'd*Ebsm*kYLSjztGK8+zwHC=FR';

GRANT ALL ON `open_bearing`.* TO 'openbearing_admin';
GRANT ALL ON open_bearing.* TO 'openbearing_admin';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `open_bearing`.`manufacturers`
-- -----------------------------------------------------
START TRANSACTION;
USE `open_bearing`;
INSERT INTO `open_bearing`.`manufacturers` (`id`, `name`) VALUES (1, 'FAG');

COMMIT;


-- -----------------------------------------------------
-- Data for table `open_bearing`.`bearings`
-- -----------------------------------------------------
START TRANSACTION;
USE `open_bearing`;
INSERT INTO `open_bearing`.`bearings` (`id`, `code`, `bpfi`, `bpfo`, `bsf`, `ftf`, `manufacturerid`) VALUES (1, '2205', 8.605, 7.952, 2.405, 0.410, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `open_bearing`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `open_bearing`;
INSERT INTO `open_bearing`.`users` (`id`, `email`, `apikey`, `enabled`) VALUES (1, 'francorosatti@gmail.com', '1234', 1);

COMMIT;

