﻿Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Return View()
    End Function

    <HttpGet>
    Function BearingReport() As ActionResult
        Return View()
    End Function

    <HttpPost>
    Function BearingReport(value As Rodamiento) As ActionResult
        LogService.Log(value)
        Return RedirectToAction("ThanksReport")
    End Function

    ' GET: Home/Request
    Function BearingRequest() As ActionResult
        Return View()
    End Function

    ' POST: Home/Request
    <HttpPost()>
    Function BearingRequest(ByVal collection As FormCollection) As ActionResult
        Try
            ' TODO: Add insert logic here

            Return RedirectToAction("Index")
        Catch
            Return View()
        End Try
    End Function

    Function ThanksReport() As ActionResult
        Return View()
    End Function

    Function ThanksRequest() As ActionResult
        Return View()
    End Function

    Function Contact() As ActionResult
        Return View()
    End Function

    Function About() As ActionResult
        Return View()
    End Function
End Class
