﻿'**********************************************************************
'* Name: Data.vb
'* Desc: Capa de servicios para la base de datos.
'* Auth: Franco Rosatti
'* Date: Nov-2018
'* Vers: 1.0
'**********************************************************************

Imports System.IO
Imports System.Data.OleDb

Public Class DBServices
#Region "Public"
    Private Shared m_LastError As String = Nothing
    ''' <summary>
    ''' Obtiene el último mensaje de error generado por la librería.
    ''' </summary>
    Public Shared Function GetLastError() As String
        Return m_LastError
    End Function
    ''' <summary>
    ''' Crea una conexión y abre la base de datos.
    ''' </summary>
    ''' <returns><bold>True</bold> si la conexión se realizó correctamente; <bold>False</bold> si ocurrió algun error.</returns>
    ''' <remarks>En caso de error llamar a <see cref="GetLastError()"/> para consultar sobre el error ocurrido.</remarks>
    Public Shared Function Open() As Boolean
        'Dim connStr As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0 ;Data Source={0} Password={1};", "C:\Users\Alejandro\Desktop\MAINTraq-Rodamientos.mdb", "simple")
        Dim connStr As String = String.Format("Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source={0}; JET OLEDB:DataBase Password={1};", Directory.GetCurrentDirectory + "\MAINTraq-Rodamientos.mdb", "simple")
        If Not DataBaseService.Open(connStr) Then
            m_LastError = DataBaseService.GetLastError()
            Return False
        End If
        Return True
    End Function
    ''' <summary>
    ''' Cierra la conexión con la base de datos.
    ''' </summary>
    ''' <returns><bold>True</bold> si la conexión se cerró correctamente; <bold>False</bold> si ocurrió algun error.</returns>
    ''' <remarks>En caso de error llamar a <see cref="GetLastError()"/> para consultar sobre el error ocurrido.</remarks>
    Public Shared Function Close() As Boolean
        If Not DataBaseService.Close() Then
            m_LastError = DataBaseService.GetLastError()
            Return False
        End If
        Return True
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns><bold>True</bold> si la transacción se inició correctamente; <bold>False</bold> si ocurrió algun error.</returns>
    ''' <remarks>En caso de error llamar a <see cref="GetLastError()"/> para consultar sobre el error ocurrido.</remarks>
    Public Shared Function BeginTransaction() As Boolean
        If Not DataBaseService.BeginTransaction() Then
            m_LastError = DataBaseService.GetLastError()
            Return False
        End If
        Return True
    End Function
    Public Shared Function RollBack() As Boolean
        If Not DataBaseService.Rollback() Then
            m_LastError = DataBaseService.GetLastError()
            Return False
        End If
        Return True
    End Function
    Public Shared Function Commit() As Boolean
        If Not DataBaseService.Commit() Then
            m_LastError = DataBaseService.GetLastError()
            Return False
        End If
        Return True
    End Function
#Region "Rodamientos"
    Public Shared Function GetRodamientosLike(filtro As String) As List(Of Rodamiento)
        If filtro Is Nothing Then filtro = ""
        filtro.Trim(" ")
        Dim result As New List(Of Rodamiento)
        Dim dr As OleDbDataReader
        Try
            Dim filter As String = ""
            For Each str As String In filtro.Split(" ")
                If str Is Nothing OrElse str.Length <= 0 OrElse str = "" Then Continue For
                If filter <> "" Then filter += "And"
                filter += "(R.Nombre Like " + DataBaseService.SetCampo("%" + str + "%") + " Or F.Nombre Like " + DataBaseService.SetCampo("%" + str + "%") + ")"
            Next
            Dim qry As String = ("SELECT R.*, F.Nombre FROM Rodamientos AS R LEFT JOIN Fabricantes AS F ON (R.Fabricante=F.Fabricante) WHERE (" + filter + ") ORDER BY F.Nombre,R.Nombre;")
            'qry = qry.Replace("$Filtro", DataBaseService.SetCampo("%" + filtro + "%"))
            dr = DataBaseService.ExecReader(qry)
        Catch ex As Exception
            dr = Nothing
        End Try
        If dr Is Nothing Then
            m_LastError = DataBaseService.GetLastError()
            Return Nothing
        End If
        Try
            While dr.Read()
                result.Add(ReadRodamiento(dr))
            End While
        Catch ex As Exception
            m_LastError = ex.Message
            result = Nothing
        End Try
        dr.Close()
        Return result
    End Function
#End Region
#End Region
#Region "Private"
    Private Shared Function ReadRodamiento(dr As OleDbDataReader) As Rodamiento
        Dim result As New Rodamiento
        result.Codigo = DataBaseService.GetCampo_String(dr.Item("R.Nombre"))
        result.Fabricante = DataBaseService.GetCampo_String(dr.Item("F.Nombre"))
        result.BPFI = DataBaseService.GetCampo_Single(dr.Item("FactorBpfi"))
        result.BPFO = DataBaseService.GetCampo_Single(dr.Item("FactorBpfo"))
        result.BSF = DataBaseService.GetCampo_Single(dr.Item("FactorBsf"))
        result.FTF = DataBaseService.GetCampo_Single(dr.Item("FactorFtf"))
        Return result
    End Function
#End Region
End Class