﻿'**********************************************************************
'* Name: DataBaseService.vb
'* Desc: Servicios relacionados con las Bases de Datos
'* Auth: Franco Rosatti
'* Date: May-2016
'* Vers: 1.1
'**********************************************************************

Imports System.Globalization
Imports System.Data.OleDb

Friend Class DataBaseService
#Region "Variables"
    Friend Shared Connection As OleDbConnection

    Private Shared Command As OleDbCommand
    Private Shared NFI As New NumberFormatInfo
    Private Shared DFI As New DateTimeFormatInfo
    Private Shared T As OleDbTransaction = Nothing
    Private Shared TS As New Stack(Of OleDbTransaction)

    Private Shared m_LastError As String = Nothing
    Friend Shared Function GetLastError() As String
        Return m_LastError
    End Function
#End Region
#Region "Métodos Friend"
    Friend Shared Function Open(connectionString As String) As Boolean
        NFI.NumberDecimalSeparator = "."
        NFI.PerMilleSymbol = ""
        NFI.NegativeSign = "-"
        DFI.ShortDatePattern = ("yyyy-MM-dd")
        DFI.LongTimePattern = "HH:mm:ss"

        Try
            Connection = New OleDbConnection
            If Connection Is Nothing Then
                m_LastError = "No fue posible crear la conexión con la base de datos."
                Return False
            End If
            Connection.Close()
            Connection.ConnectionString = connectionString
            Connection.Open()
            If Connection.State <> ConnectionState.Open Then
                m_LastError = "No fue posible abrir la conexión con la base de datos."
                Return False
            End If
            Return True
        Catch ex As Exception
            m_LastError = ex.Message
        End Try
        Return False
    End Function
    Friend Shared Function Close() As Boolean
        If Connection Is Nothing Then
            m_LastError = "No existe una conexión con la base de datos."
            Return False
        End If
        Try
            Connection.Close()
        Catch ex As Exception
            m_LastError = ex.Message
            Return False
        End Try
        If Connection.State <> ConnectionState.Closed Then
            m_LastError = "No fue posible cerrar la conexión con base de datos."
            Return False
        End If
        Return True
    End Function
    Friend Shared Function BeginTransaction() As Boolean
        Try
            TS.Push(T)
            T = Connection.BeginTransaction()
            Return True
        Catch ex As Exception
            m_LastError = ex.Message
            Return False
        End Try
    End Function
    Friend Shared Function Commit() As Boolean
        Try
            If T Is Nothing Then
                m_LastError = "No existe ninguna transacción pendiente."
                Return False
            End If
            T.Commit()
            T = TS.Pop()
            Return True
        Catch ex As Exception
            m_LastError = ex.Message
            Return False
        End Try
    End Function
    Friend Shared Function Rollback() As Boolean
        Try
            If T Is Nothing Then
                m_LastError = "No existe ninguna transacción pendiente."
                Return False
            End If
            T.Rollback()
            T = TS.Pop
            Return True
        Catch ex As Exception
            m_LastError = ex.Message
            Return False
        End Try
    End Function
    Friend Shared Function ExecNonQuery(ByVal strcmd As String) As Integer
        Try
            Command = New OleDbCommand(strcmd, Connection)
            If Command Is Nothing Then
                m_LastError = "No fue posible crear la consulta a la base de datos"
                Return -1
            End If
            If T IsNot Nothing Then Command.Transaction = T
            Return Command.ExecuteNonQuery()
        Catch ex As Exception
            m_LastError = ex.Message
            Return -1
        End Try
    End Function
    Friend Shared Function ExecReader(ByVal strcmd As String) As OleDbDataReader
        Try
            Command = New OleDbCommand(strcmd, Connection)
            If Command Is Nothing Then
                m_LastError = "No fue posible crear la consulta a la base de datos"
                Return Nothing
            End If
            If T IsNot Nothing Then Command.Transaction = T
            Return Command.ExecuteReader()
        Catch ex As Exception
            m_LastError = ex.Message
            Return Nothing
        End Try
    End Function
    Friend Shared Function ExecScalar(ByVal strcmd As String) As Object
        Try
            Command = New OleDbCommand(strcmd, Connection)
            If Command Is Nothing Then
                m_LastError = "No fue posible crear la consulta a la base de datos"
                Return Nothing
            End If
            If T IsNot Nothing Then Command.Transaction = T
            Return Command.ExecuteScalar()
        Catch ex As Exception
            m_LastError = ex.Message
            Return Nothing
        End Try
    End Function
#Region "LEER CAMPOS: Transforma los datos de la BD a las entidades"
    Friend Shared Function GetCampo_Bool(ByVal value As Object) As Boolean
        Return value
    End Function
    Friend Shared Function GetCampo_Int(ByVal value As Object) As Integer
        If value Is Nothing Then Return -1
        If value Is DBNull.Value Then Return -1
        Return value
    End Function
    Friend Shared Function GetCampo_Date(ByVal value As Object) As Date
        If value Is DBNull.Value Then Return Date.MinValue
        Return value
    End Function
    Friend Shared Function GetCampo_DateTime(ByVal value As Object) As DateTime
        If value Is DBNull.Value Then Return DateTime.MinValue
        Return value
    End Function
    Friend Shared Function GetCampo_String(ByVal value As Object) As String
        If value Is DBNull.Value Then Return Nothing
        Return value
    End Function
    Friend Shared Function GetCampo_Single(ByVal value As Object) As Single
        If value Is DBNull.Value Then Return -1
        Return value
    End Function
#End Region
#Region "ESCRIBIR CAMPOS: Transforma los datos a la forma utilizada por la BD"
    Friend Shared Function SetCampo(ByVal value As Boolean) As String
        Return value.ToString
    End Function
    Friend Shared Function SetCampo(ByVal value As Integer) As String
        If value = -1 Then Return "null"
        Return value.ToString
    End Function
    Friend Shared Function SetCampo(ByVal value As DateTime) As String
        If value = DateTime.MinValue Then Return "null"
        Return "'" + value.ToString(DFI) + "'"
    End Function
    Friend Shared Function SetCampo(ByVal value As String) As String
        If value = "" Then Return "null"
        Return "'" + value + "'"
    End Function
    Friend Shared Function SetCampo(ByVal value As Single) As String
        If value = -1 Then Return "null"
        Return value.ToString(NFI)
    End Function
#End Region
#End Region
End Class