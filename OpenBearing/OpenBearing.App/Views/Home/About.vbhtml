﻿@Code
    ViewData("Title") = "Open Bearing - About"
    Layout = Nothing
End Code

<style>
    * {
        font-family: 'Open Sans', sans-serif;
        font-size: 24px;
    }

    body {
        background-color: black;
    }

    #about {
        margin-top: 50px;
        text-align: center;
        color: #fff;
    }
</style>

<div id="about">
    <a href="/"><img src="~/Images/bearing.png" /></a>
    <p>Open Bearing is a collaborative project.</p>
    <p>Feel free to <a href="/Home/Contact">contact us</a> to make this project bigger.</p>
</div>
