﻿@Code
    ViewData("Title") = "Open Bearing - Contact us"
    Layout = Nothing
End Code

<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">

<style>
    * {
        font-family: 'Open Sans', sans-serif;
        font-size: 24px;
    }

    body {
        background-color: black;
    }

    #contact {
        margin-top: 50px;
        text-align: center;
        color: #fff;
    }
</style>

<div id="contact">
    <a href="/"><img src="~/Images/bearing.png" /></a>
    <p>Franco Rosatti</p>
    <p>francorosatti@gmail.com</p>
    <p>&copy; @DateTime.Now.Year - Buenos Aires - Argentina</p>
</div>