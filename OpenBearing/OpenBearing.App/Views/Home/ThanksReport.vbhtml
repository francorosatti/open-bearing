﻿@Code
    ViewData("Title") = "Thanks"
    Layout = Nothing
End Code

<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">

<style>
    * {
        font-family: 'Open Sans', sans-serif;
    }

    h2 {
        margin-top: 50px;
        font-size: 36px;
        text-align: center;
    }

    p {
        display: inline;
    }
</style>

<h2>Thanks for reporting the problem, it will be fixed soon.</h2>
<h2>You're being redirected in <p id="secs">5</p></h2>

<script>
    var s = document.getElementById('secs');
    var i = 5;

    var _timer = setInterval(function () {
        if (i > 0) {
            i--;
        }
        else {
            clearInterval(_timer);
            window.location.href = '/';
        }
        s.innerHTML = i;
    }, 1000);
</script>