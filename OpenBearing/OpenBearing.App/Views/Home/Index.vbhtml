﻿@Code
    ViewData("Title") = "Open Bearing"
End Code

<script src="~/Scripts/jquery.debounce-1.0.5.js"></script>

<style>
    #bearing-search {
        margin-top: 150px;
        margin-bottom: 150px;
    }
</style>
<div class="row with-margin">
    <div class="col-lg-12">
        <div class="input-group-lg">
            <input type="text" class="form-control input-lg" id="bearing-search" placeholder="Search bearing code or manufacturer...">
        </div><!-- /input-group -->
    </div><!-- /.col-lg-6 -->
</div><!-- /.row -->

<script>
    $(document).ready(function () {
        $('#bearing-search').keyup($.debounce(function () {
            alert($(this).val());
        }, 500));
    });


</script>