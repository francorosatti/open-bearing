﻿'**********************************************************************
'* Name: Entidades.vb
'* Desc: Entidades utilizadas en la aplicación.
'* Auth: Franco Rosatti
'* Date: Nov-2018
'* Vers: 1.0
'**********************************************************************

Public Class Rodamiento
    Public Property Codigo As String
    Public Property Fabricante As String
    Public Property BPFI As Single = 0
    Public Property BPFO As Single = 0
    Public Property BSF As Single = 0
    Public Property FTF As Single = 0
End Class

Public Class Reporte
    Public Property Codigo As String
    Public Property Fabricante As String
    Public Property Mensaje As String
End Class